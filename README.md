# myBotDiscord

Need to have a config.json file like the following:

{
    "prefix": "your prefix (ex: !cara)",
    "token": "bot's token",
    "ownerID": "your id",
    "whiteList": ["authorised_ID_1", "authorised_ID_2"]

}